const express = require('express');
const router = express.Router();
const sqlite3 = require('sqlite3').verbose();
router.use(express.urlencoded({ extended: true }));
router.use(express.json());

const db = new sqlite3.Database('C:/sqlite/sqlitetools/mydatabase.db');

db.serialize(() => {
  db.run('CREATE TABLE IF NOT EXISTS UsersTable (id INTEGER PRIMARY KEY, username TEXT UNIQUE, variableMail TEXT UNIQUE, password TEXT)');
});

router.get('/', (req, res) => {
  res.render('index', { displayMessage: undefined });
});

router.get('/users', (req, res) => {
  res.render('usermain', { displayMessage: undefined });
});





router.get('/users/signup', (req, res) => {
  res.render('signup', { displayMessage: undefined });
});

router.get('/users/login', (req, res) => {
  res.render('login', { displayMessage: undefined, displayUsername: undefined });
});

router.post('/users/signup', async (req, res) => {

  const username = req.body.username;
  const password = req.body.password;
  const variableMail = req.body.variableMail;

  const query = 'SELECT username, variableMail FROM UsersTable';
  db.all(query, async (err, rows) => {
    if (err) {
      console.error(err.message);
    }
    let userExists = rows.some((row) => row.username === username);
    let emailExists = rows.some((row) => row.variableMail === variableMail);
    let message = '';

    if (userExists || emailExists) {
      if (userExists) {
          message += 'User ';
      }
      if (emailExists) {
        message += 'Mail ';
      }
      message += 'already existing';

      res.render('signup', { displayMessage: message });
    }
    else {
      await db.run('INSERT INTO UsersTable (username, variableMail, password) VALUES (?, ?, ?)', [username, variableMail, password], (err) => {
        if (err) {
          console.error(err);
          console.log('error');
        } else {
          res.render('signup', { displayMessage: '200' });
        }
      })
    }
  })
})

router.post('/users/login', async (req, res) => {
  const username = req.body.username;
  const password = req.body.password;

  const query = 'SELECT * FROM UsersTable';
  await db.all(query, async (err, rows) => {
    if (err) {
      console.error(err);
      return res.status(500).send('Internal Server Error');
    }

    let userValidated = rows.some((row) => row.username === username && row.password === password);

    const query2 = 'SELECT username FROM UsersTable WHERE username = ?'
    await db.get(query2, [username], (err, row) => {
      if (err) {
        console.error(err);
        return res.status(500).send('Internal Server Error');
      }
      let DBusername = row ? row.username : null;

      if (!userValidated) {
        res.render('login', { displayMessage: '400', displayUsername: DBusername });
      } else {
        res.render('login', { displayMessage: '200', displayUsername: DBusername });
      }
    })
  });
});

module.exports = router


